## 약품관리 Android App

### * 공공 데이터 오픈 API이용 
> * 활용 데이터 : 
> >'식품의약품안전처_의약품 성분별 1일 최대투여량 정보'

### 기능
> * 약품 정보 리스트
> * 키워드 검색 필터

---

## Page_Index
> ##
> <center><img src="./images/index.jpg" width="450" height="900"></center>
>

## Loading
> ##
> <center><img src="./images/loading.jpg" width="450" height="900"></center>
>

## Page_Search
> ##
> <center><img src="./images/search_page.jpg" width="450" height="900"></center>
>
## Page_Result
> ##
> <center><img src="./images/search_result.jpg" width="450" height="900"></center>
> 
