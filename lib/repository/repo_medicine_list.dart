import 'dart:convert';

import 'package:app_api_medicine/config/config_api.dart';
import 'package:app_api_medicine/model/medicine_list.dart';
import 'package:app_api_medicine/model/medicine_response.dart';
import 'package:dio/dio.dart';

class RepoMedicineList {
  final String _baseUrl = '$apiMedicine/1471000/DayMaxDosgQyByIngdService/getDayMaxDosgQyByIngdInq'; // 물음표 전까지

  Future<MedicineList> getList({int page = 1, String CPNT_CD = '', String DRUG_CPNT_KOR_NM = '', String DRUG_CPNT_ENG_NM = ''})
  async { // 비동기식(async) + 데이터 인풋 아웃풋 + 파라미터 / {int page = 1}은 무한스크롤때 추가

    Dio dio = Dio();

    Map<String, dynamic> params = {}; // Future ctrl찍고 들어가보면 보이는 형태 + {json넣어주기}
    params['pageNo'] = page;
    params['serviceKey'] = Uri.encodeFull(apiMedicineKey); // 디코딩 된 키를 인코딩 해준다.
    params['type'] = 'json'; // 문서에서 봤던 포스트맨에 필요했던 type
    if (CPNT_CD != '') params['CPNT_CD'] = Uri.encodeFull(CPNT_CD);
    if (DRUG_CPNT_KOR_NM != '') params['DRUG_CPNT_KOR_NM'] = Uri.encodeFull(DRUG_CPNT_KOR_NM);
    if (DRUG_CPNT_ENG_NM != '') params['DRUG_CPNT_ENG_NM'] = Uri.encodeFull(DRUG_CPNT_ENG_NM);

    final response = await dio.get(
      _baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false, // 따라갈거야? + validateStatus 는 여기선 필요없음, 왜냐하면 실패해도 200이고 성공해도 200으로 주고있어서..
      )
    ); // 성공과 실패가 애매하기 때문에 바로 return을 쓰지 않고
    MedicineResponse resultModel = MedicineResponse.fromJson(response.data);
    return resultModel.body!; //메디슨 리스트가 없을 수도 있는데 어떻게 할건지에 대해 쓴다. + 느낌표 추가! 스테이터스 코드를 주지않아서 이런 것..
  }
}