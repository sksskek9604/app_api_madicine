import 'package:app_api_medicine/pages/page_index.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '의약품 정보',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate, // 무언가 설정에 대한걸 배달하는 느낌
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ], // 앱 전체를 통틀어서 글로벌세팅이되는 것(앱 전체를 관장하는 최상위라서 main에 앱틀에다가 추가한 것),
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const PageIndex(),
    );
  }
}