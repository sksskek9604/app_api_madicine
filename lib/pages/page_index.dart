import 'package:app_api_medicine/components/component_appbar_filter.dart';
import 'package:app_api_medicine/components/component_count_title.dart';
import 'package:app_api_medicine/components/component_cutom_loading.dart';
import 'package:app_api_medicine/components/component_list_item_form.dart';
import 'package:app_api_medicine/components/component_no_contents.dart';
import 'package:app_api_medicine/model/medicine_list_item.dart';
import 'package:app_api_medicine/pages/page_search.dart';
import 'package:app_api_medicine/repository/repo_medicine_list.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}
// AutomaticKeepAliveClientMixin : 한번 뿌렸던 화면 정보 저장.

class _PageIndexState extends State<PageIndex> {
  final _scrollController = ScrollController();

  List<MedicineListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _CPNT_CD = '';
  String _DRUG_CPNT_KOR_NM = '';
  String _DRUG_CPNT_ENG_NM = '';

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      } // 바닥에 부딪치면 로드아이템 실행시켜라
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    // 리스트 안에 리스트가 들어감
    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });
      await RepoMedicineList()
          .getList(
          page: _currentPage,
          CPNT_CD: _CPNT_CD,
          DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
          DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)
          .then((res) => {
      BotToast.closeAllLoading(),

      setState(() {
      _totalPage = res.totalPage;
      _totalItemCount = res.totalItemCount;

      _list = [..._list, ...res.items];

      _currentPage++;
      })
      }).catchError((err) => BotToast.closeAllLoading());
      if (reFresh) {
        _scrollController.animateTo(0,
            duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    opacity: 0.88,
                    fit: BoxFit.fill,
                    image: AssetImage('assets/medicine_basil.jpg'))),
            child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: ComponentAppbarFilter(
                  title: '의약품 정보',
                  actionIcon: Icons.search,
                  callback: () async {
                    final searchResult = await Navigator
                        .push( // push(열면서), await (기다린다)
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PageSearch(
                                    CPNT_CD: _CPNT_CD,
                                    DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
                                    DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)));

                    if (searchResult != null &&
                        searchResult[0]) { // 첫번째 말이 [true ~] 참이면,
                      // '아! (page_search)FormBuilder에서 넘긴 값, 나한테 뭐 가져가라고 말해줬구나' 해서 변수에 집어넣고 다시 loadItems(reFresh)를 불러온다.
                      // (reFresh모드? - 초기화 한번 하고, 스크롤을 맨 위로 올린다. )
                      _CPNT_CD = searchResult[1];
                      _DRUG_CPNT_KOR_NM = searchResult[2];
                      _DRUG_CPNT_ENG_NM = searchResult[3];
                      _loadItems(reFresh: true);
                    }
                  },
                ),
                body: ListView(
                  controller: _scrollController,
                  children: [
                    // 항시 보여지도록 이 안에다가 넣음 ComponentsCountTitle
                    ComponentCountTitle(
                        icon: Icons.add_alert,
                        count: _totalItemCount,
                        unitName: '건',
                        itemName: '의약품 정보'),
                    _buildBody()
                  ],
                ))),
      ],
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            // 변수로 값을 받고 있기 때문에 빌더를 받는다.
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length, // 리스트 몇개 그릴거야? = 있는만큼만 시도해야지
            itemBuilder: (_, index) =>
                ComponentListItemForm(item: _list[index], callback: () {}),
          )
        ],
      ); // 나중에 배너 자리 등을 위해서라도 Column으로 한번 씌운다.
    } else {
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height -
            50 -
            20, // 기본 -50 ComponentsCountTitle 영역까지 빼기 -20
        child: const ComponentNoContents(
            icon: Icons.add_alert, msg: '의약품 정보가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeppAlive => true;
}
