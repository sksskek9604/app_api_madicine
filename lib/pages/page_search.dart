import 'package:app_api_medicine/components/component_appbar_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageSearch extends StatefulWidget {
  PageSearch(
      {super.key,
      this.CPNT_CD = '',
      this.DRUG_CPNT_KOR_NM = '',
      this.DRUG_CPNT_ENG_NM = ''});

  String CPNT_CD;
  String DRUG_CPNT_KOR_NM;
  String DRUG_CPNT_ENG_NM;

  @override
  State<PageSearch> createState() => _PageSearchState();
}

class _PageSearchState extends State<PageSearch> {
  final GlobalKey<FormBuilderState> _formkey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '의약품 상세 검색'),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: OutlinedButton(
            // 검색하기 버튼 -> 각각의 값을 받아와서
            onPressed: () {
              String inputCPNT_CD =
                  _formkey.currentState!.fields['CPNT_CD']!.value;
              String inputDRUG_CPNT_KOR_NM =
                  _formkey.currentState!.fields['DRUG_CPNT_KOR_NM']!.value;
              String inputDRUG_CPNT_ENG_NM =
                  _formkey.currentState!.fields['DRUG_CPNT_ENG_NM']!.value;

              Navigator.pop(
                // pop으로 닫는 데, 정보를 주고 있다. (나 검색버튼 눌렀다)
                context,
                [
                  true,
                  inputCPNT_CD,
                  inputDRUG_CPNT_KOR_NM,
                  inputDRUG_CPNT_ENG_NM
                ],
              ); // 내가 넘겨줄 정보를 나열함, true부터 0번째
            },
            child: const Text(
              '검색하기',
              style: TextStyle(
                  fontFamily: 'font_KR',
                  fontSize: 25.5,
                  fontWeight: FontWeight.w800),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        FormBuilder(
          key: _formkey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                FormBuilderTextField(
                  name: 'CPNT_CD',
                  initialValue: widget.CPNT_CD,
                  // 생성자 받은 이유는 검색 내용이 창이 닫혀도, 열었을 때 남아있도록 하기 위함이다.
                  decoration: const InputDecoration(labelText: '성분코드'),
                  style: TextStyle(fontFamily: 'font_KR', fontSize: 35),
                ),
                SizedBox(height: 30),
                FormBuilderTextField(
                  name: 'DRUG_CPNT_KOR_NM',
                  initialValue: widget.DRUG_CPNT_KOR_NM,
                  // initialValue = 기본으로 들어가 있을 값
                  decoration: const InputDecoration(labelText: '성분명(한글)'),
                  style: TextStyle(fontFamily: 'font_KR', fontSize: 35),
                ),
                SizedBox(height: 30),
                FormBuilderTextField(
                  name: 'DRUG_CPNT_ENG_NM',
                  initialValue: widget.DRUG_CPNT_ENG_NM,
                  // initialValue = 기본으로 들어가 있을 값
                  decoration: const InputDecoration(labelText: '성분명(영문)'),
                  style: TextStyle(fontFamily: 'font_KR', fontSize: 35),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
