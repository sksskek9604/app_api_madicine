import 'package:app_api_medicine/model/medicine_list.dart';

class MedicineResponse {
  MedicineList? body; // 에러나면 바디가 없기 때문에 물음표를 달아줍니다.

  MedicineResponse({this.body});

  factory MedicineResponse.fromJson(Map<String, dynamic> json) {
    return MedicineResponse(
      body: json['body'] == null ? null : MedicineList.fromJson(json['body']),
    );
  }
}