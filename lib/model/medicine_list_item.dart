
class MedicineListItem{
  String CPNT_CD; //성분 코드
  String DRUG_CPNT_KOR_NM; //성분명(한글)
  String DRUG_CPNT_ENG_NM; //성분명(영문)
  String FOML_CD; //제형코드
  String FOML_NM; //제형 명
  String DOSAGE_ROUTE_CODE; //투여 경로
  String DAY_MAX_DOSG_QY_UNIT; //투여 단위
  String DAY_MAX_DOSG_QY;
  //1일 최대 투여량

  MedicineListItem(
      this.CPNT_CD,
    this.DRUG_CPNT_KOR_NM,
    this.DRUG_CPNT_ENG_NM,
    this.FOML_CD,
    this.FOML_NM,
    this.DOSAGE_ROUTE_CODE,
    this.DAY_MAX_DOSG_QY_UNIT,
    this.DAY_MAX_DOSG_QY
  );

  factory MedicineListItem.fromJson(Map<String, dynamic> json) {
    // nullable로 했던 부분을, 여기서 초기화를 하겠음. 기본값을 주겠음
    return MedicineListItem(
      json['CPNT_CD'] == null ? '성분 코드 값이 없습니다. ' : json['CPNT_CD'],
      json['DRUG_CPNT_KOR_NM'] == null ? '약품의 한글이름이 없습니다.' : json['DRUG_CPNT_KOR_NM'],
      json['DRUG_CPNT_ENG_NM'] == null ? '약품의 영문이름이 없습니다.' : json['DRUG_CPNT_ENG_NM'],
      json['FOML_CD'] == null ? '제형코드가 없습니다.' : json['FOML_CD'],
      json['FOML_NM'] == null ? '제형 명이 없습니다.' : json['FOML_NM'],
      json['DOSAGE_ROUTE_CODE'] == null ? '투여 경로 데이터가 없습니다. ' : json['DOSAGE_ROUTE_CODE'],
      json['DAY_MAX_DOSG_QY_UNIT'] == null ? '투여단위 데이터가 없습니다.' : json['DAY_MAX_DOSG_QY_UNIT'],
      json['DAY_MAX_DOSG_QY'] == null ? '1일 최대 투여량 값이 없습니다.' : json['DAY_MAX_DOSG_QY'],
    );
  }
}
