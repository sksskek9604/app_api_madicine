import 'package:app_api_medicine/model/medicine_list_item.dart';

class MedicineList {
  // 타입을 int로 준 것 포스트맨에서 확인, 이따금 ""안에 출력이 될 경우가 있으니 주의!
  // 그리고 totalPage를 추가+ 변수명도 알아보기 쉽도록 바꾼다.
  int currentPage;
  int totalPage;
  int totalItemCount;
  List<MedicineListItem> items;

  MedicineList(this.currentPage, this.totalPage, this.totalItemCount, this.items);

  factory MedicineList.fromJson(Map<String, dynamic> json) {
    // 팩토리에서 가공 중

    int totalItemCount = json['totalCount'] == null ? 0 : json['totalCount'] as int;
    int numOfRows = json['numOfRows'] == null ? 10 : json['numOfRows'] as int;
    int totalPage = (totalItemCount / numOfRows).ceil(); // 총 페이지 수 계산식


    return MedicineList(
      // 생성자 호출 및 해당 내용 계산
        json['pageNo'] == null ? 1 : json['pageNo'] as int,
        (json['totalCount'] != null && json['numOfRows'] != null) ? totalPage : 1,
        json['totalCount'] == null ? 1 : json['totalCount'] as int,
        json['items'] == null ? [] : (json['items'] as List).map((e) => MedicineListItem.fromJson(e)).toList());
    // 인자를 e로 주고 다시 리스트로 변환.
  }
}