import 'package:flutter/material.dart';
class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({super.key, required this.icon, required this.count, required this.unitName, required this.itemName});

  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fitWidth,
            opacity: 0.9,
            image: AssetImage('assets/count_banner.png')
          ),
        ),
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon, size: 30),
            const SizedBox(width: 10,
            ),
            Text(
              '총 ${count.toString()}${unitName}의 ${itemName}이(가) 있습니다.',
              style: TextStyle(fontFamily: 'font_KR',fontSize: 16, fontWeight: FontWeight.w600),)
          ],
        ),
      ),
    );
  }
}
