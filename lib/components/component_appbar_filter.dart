import 'package:flutter/material.dart';

class ComponentAppbarFilter extends StatefulWidget implements PreferredSizeWidget{
  const ComponentAppbarFilter({super.key, required this.title, required this.actionIcon, required this.callback});

  final String title;
  final IconData actionIcon;
  final VoidCallback callback;

  @override
  State<ComponentAppbarFilter> createState() => _ComponentAppbarFilterState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(40); // 위젯 높이를 30dp로 고정
  }
}

class _ComponentAppbarFilterState extends State<ComponentAppbarFilter> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false, // 타이틀(글자) 앞에 자동으로 뒤로가기버튼 같은거 달리는거 허용여부
      title: Text(widget.title, style: TextStyle(fontFamily: 'font_KR', fontSize: 28),),
      elevation: 1.0, // 앱바 아래 그림자
      actions: [
        IconButton(onPressed: widget.callback, icon: Icon(widget.actionIcon)),
      ],
    );
  }
}
