import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class ComponentCustomLoading extends StatefulWidget {
  const ComponentCustomLoading({super.key, required this.cancelFunc});

  final CancelFunc cancelFunc;


  @override
  State<ComponentCustomLoading> createState() => _ComponentCustomLoadingState();
}

class _ComponentCustomLoadingState extends State<ComponentCustomLoading> with SingleTickerProviderStateMixin{
  //@@@controller라고 하면 무언가 도와주는 느낌, animation은 움직이도록 도와주는!

  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
      duration: const Duration(
        milliseconds: 1000,
      ),
      vsync: this,
    );

    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animationController.forward();
      }
    });
    animationController.forward();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white70,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 16,
          bottom: 16,
          left: 28,
          right: 28,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FadeTransition(
              opacity: animationController,
              child: Image.asset(
                'assets/loading.png',
                width: 120,
              ),
            ),
            SizedBox(height: 10,),
            Text('Loading...♧', style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600, fontFamily: 'font_KR'),)
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
