import 'package:flutter/material.dart';

class ComponentAppbarPopup extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarPopup({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      iconTheme: const IconThemeData(
        color: Colors.black45,
      ),
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: const TextStyle(
            fontFamily: 'font_KR', color: Colors.black45, fontSize: 20),
      ),
      elevation: 1,
      actions: [
        IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.clear))
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(40);
}
