import 'package:app_api_medicine/model/medicine_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItemForm extends StatelessWidget {
  const ComponentListItemForm({super.key, required this.item, required this.callback });

  final MedicineListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          image: DecorationImage(
            opacity: 0.89,
            fit: BoxFit.cover,
            image: AssetImage('assets/list_item_img.png')
          ),
          border: Border.all(color: const Color.fromRGBO(300, 300, 300, 100)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('성분 코드: ' + '${item.CPNT_CD}', style: TextStyle(fontFamily: 'font_KR',fontSize: 20, fontWeight: FontWeight.bold),),
            Text('성분명(한글): ' + '${item.DRUG_CPNT_KOR_NM}', style: TextStyle(fontFamily: 'font_KR', fontSize: 16, fontWeight: FontWeight.bold, color: Colors.green)),
            Text('성분명(영문): ' + '${item.DRUG_CPNT_ENG_NM}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
            Text('제형 코드: ' + '${item.FOML_CD}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
            Text('제형명: ' + '${item.FOML_NM}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
            Text('투여 경로: ' + '${item.DOSAGE_ROUTE_CODE}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
            Text('투여 단위: ' + '${item.DAY_MAX_DOSG_QY_UNIT}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
            Text('1일 최대 투여량: ' + '${item.DAY_MAX_DOSG_QY}', style: TextStyle(fontFamily: 'font_KR', fontWeight: FontWeight.w500)),
          ],
        ),
      ),
    );
  }
}
